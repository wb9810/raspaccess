#!/usr/bin/env python
# -*- coding: utf8 -*-

import RPi.GPIO as GPIO
import MFRC522
import signal
import requests
import time
import grovepi
import configparser

import Adafruit_GPIO.SPI as SPI
import Adafruit_SSD1306

from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont

import subprocess

# Load INI File
config = configparser.ConfigParser()
config.read('/var/www/html/raspaccess/application/config/config.ini')
print('Config readed...')

# Define script VARS
continue_reading = True
url = config['service']['server']+config['service']['api']
print('URL setted...')

# Initialize relay
#relay = config['service']['relay']
relay = 3
grovepi.pinMode(relay,"OUTPUT")
print('Relay initialized...')

# ************************************** Display Initialization ************************************** 
# Raspberry Pi pin configuration:
RST = None     # on the PiOLED this pin isnt used
# Note the following are only used with SPI:
DC = 23
SPI_PORT = 0
SPI_DEVICE = 0
# 128x32 display with hardware I2C:
disp = Adafruit_SSD1306.SSD1306_128_32(rst=RST)

# Initialize library.
disp.begin()

# Clear display.
disp.clear()
disp.display()

# Create blank image for drawing.
# Make sure to create image with mode '1' for 1-bit color.
width = disp.width
height = disp.height
image = Image.new('1', (width, height))

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# Draw a black filled box to clear the image.
draw.rectangle((0,0,width,height), outline=0, fill=0)

# Draw some shapes.
# First define some constants to allow easy resizing of shapes.
padding = -2
top = padding
bottom = height-padding
# Move left to right keeping track of the current x position for drawing shapes.
x = 0

# Load default font.
font = ImageFont.load_default()
# ************************************** END ************************************** 
print('Display loaded...')


# ************************************** Display Function ************************************** 
def showScreen( line1, line2):
    # Draw a black filled box to clear the image.
    draw.rectangle((0,0,width,height), outline=0, fill=0)

    # Shell scripts for system monitoring from here : https://unix.stackexchange.com/questions/119126/command-to-display-memory-usage-disk-usage-and-cpu-load
    cmd = "hostname -I | cut -d\' \' -f1"
    IP = subprocess.check_output(cmd, shell = True )
    cmd = "iwgetid -r"
    WLAN = subprocess.check_output(cmd, shell = True )

    # Write two lines of text.
    draw.text((x, top),       "IP: " + str(IP),  font=font, fill=255)
    draw.text((x, top+8),     "WLAN: "+ str(WLAN), font=font, fill=255)
    draw.text((x, top+16),    line1,  font=font, fill=255)
    draw.text((x, top+25),    line2,  font=font, fill=255)

    # Display image.
    disp.image(image)
    disp.display()
    time.sleep(.5)
    return
# ************************************** END ************************************** 
print('Screenfunction defined...')



# Capture SIGINT for cleanup when the script is aborted
def end_read(signal,frame):
    global continue_reading
    print "Ctrl+C captured, ending read."
    continue_reading = False
    GPIO.cleanup()

# Hook the SIGINT
signal.signal(signal.SIGINT, end_read)

print('Try to initialize Mifare...')
# Create an object of the class MFRC522
MIFAREReader = MFRC522.MFRC522()
print('Mifare initialized...')

# This loop keeps checking for chips. If one is near it will get the UID
while continue_reading:
    
    # Initialize
    msg = "Waiting for Card."
    print msg
    showScreen("",msg)
    
    # Scan for cards    
    (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

    # If a card is found
    if status == MIFAREReader.MI_OK:
        msg = "Card detected."
        print msg
        showScreen("",msg)
        
    # Get the UID of the card
    (status,uid) = MIFAREReader.MFRC522_Anticoll()

    # If we have the UID, continue
    if status == MIFAREReader.MI_OK:
        
        # Generate UID
        card_uid = str(uid[0])+""+str(uid[1])+""+str(uid[2])+""+str(uid[3])
        
        # Print UID
        msg = "Card UID: "+card_uid
        print msg
        showScreen("",msg)
        
	print('Contact server...')
	print url
        # Check Local Server
        payload = {'tag': card_uid}
        r = requests.post(url, data=payload)
        
        if r.status_code == 200:
            # Print card state
            msg = "Cardstate: 200"
            print msg
            print r.text
            
            # Open the door
            msg2 = "Door open."
            print msg2
            showScreen(msg,msg2)
            
            grovepi.digitalWrite(relay,1)
            time.sleep(int(config['service']['wait']))
            grovepi.digitalWrite(relay,0)
            msg = "Door closed."
            print msg
            showScreen("",msg)
            
        else:
            # Print card state
            msg = "Cardstate: "+str(r.status_code)
            msg2 = r.text
            print msg
            print msg2
            showScreen(msg,msg2)
            time.sleep(2)
