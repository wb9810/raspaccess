<table id="log_table" class="table">
    <thead>
        <tr>
            <th>Time</th>
            <th>RFID Tag</th>
            <th>State</th>
        </tr>
    </thead>
</table>


<script type="text/javascript" charset="utf8" src="./assets/js/datatables/datatables.min.js"></script>
<script>
    /* global $ */
    $(document).ready( function () {
        var table = $('#log_table').DataTable({
            "ajax": $(location).attr('protocol')+'//'+$(location).attr('hostname')+'/raspaccess/application/api/v1/accesslog/all',
            "order": [[ 0, "desc" ]],
            aoColumns: [
                { data: 'created' },
                { data: 'rfidtag' },
                { data: 'code',
                  render: function(data, type, row, meta){
                    if(data == 200) {
                      return '<i class="mdi mdi-check-circle text-success"></i> '+row.msg;
                    } else {
                        if(data == 503) {
                            return '<i class="mdi mdi-verified text-danger"></i> '+row.msg;
                        } else {
                            return '<i class="mdi mdi-alert-box text-warning"></i> '+row.msg+' <a href="'+$(location).attr('protocol')+'//'+$(location).attr('hostname')+'/raspaccess/application/access?create='+row.rfidtag+'"><i class="mdi mdi-open-in-new"></i></a>';
                        }
                    }
                }}
            ]
        });
        if( $.urlParam('rfid') ) {
            table.search( $.urlParam('rfid') ).draw();    
        }
    } );
</script>