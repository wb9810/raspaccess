<div class="header clearfix">
    <nav>
      <ul class="nav nav-pills float-right">
        <li class="nav-item">
          <a class="nav-link <?php echo @$current_logfile;?>" href="logfile">Logfile</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php echo @$current_access;?>" href="access">Access</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php echo @$current_server;?>" href="server">Server</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php echo @$current_settings;?>" href="settings">Settings <?php if($SiteState['errors']>0) { echo '<span class="badge badge-danger">'.$SiteState['errors'].'</span>';}?></a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?php echo @$current_logout;?>" href="logout">Logout</a>
        </li>
      </ul>
    </nav>
    <h3 class="text-muted"><i class="mdi mdi-verified" style="color: #ec1354;"></i> RASPaccess</h3>
</div>

