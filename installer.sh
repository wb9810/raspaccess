#!/usr/bin/env bash

# Set Install Vars
PASSWORD='raspaccess'
INSTALLFOLDER='/var/www/html/raspaccess'

# Update the system
sudo apt-get update
sudo apt-get -y upgrade

# >>>>>>>>>>>>> Install Webserver <<<<<<<<<<<<<<<<
# Install Apache & PHP
sudo apt-get install -y apache2
sudo apt-get install -y php5
# Install MySQL
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $PASSWORD"
sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $PASSWORD"
sudo apt-get -y install mysql-server
sudo apt-get install php5-mysql
# Install phpMyAdmin
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/dbconfig-install boolean true"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/app-password-confirm password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/admin-pass password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/mysql/app-pass password $PASSWORD"
sudo debconf-set-selections <<< "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2"
sudo apt-get -y install phpmyadmin
# Enable mod_rewrite
sudo a2enmod rewrite
# Restart Apache
service apache2 restart
# >>>>>>>>>>>>> End Webserver <<<<<<<<<<<<<<<<

# Install Git
sudo apt-get -y install git

# Git clone RASPaccess
sudo git clone https://bitbucket.org/wb9810/raspaccess.git "${INSTALLFOLDER}"
sudo chmod 777 /var/www/html/raspaccess/application/config/config.ini

# >>>>>>>>>>>>> Install Display <<<<<<<<<<<<<<<<
# Install Display Components
sudo apt-get install build-essential python-dev python-pip
sudo pip install RPi.GPIO
sudo apt-get install python-imaging python-smbus
git clone https://github.com/adafruit/Adafruit_Python_SSD1306.git
cd Adafruit_Python_SSD1306
sudo python setup.py install
cd ..
sudo rm -rf Adafruit_Python_SSD1306
# >>>>>>>>>>>>> End Display <<<<<<<<<<<<<<<<

# Install GrovePI
sudo curl -kL dexterindustries.com/update_grovepi | bash

# Install MFRC522
sudo git clone https://github.com/lthiery/SPI-Py.git
cd SPI-Py
sudo python setup.py install
cd ..
sudo rm -rf SPI-Py

# Install Config Parser
sudo apt-get install python-configparser

# Install Composer
sudo apt-get install curl php5-cli git
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
cd "${INSTALLFOLDER}/application"
sudo composer install

# Install Service
cd "${INSTALLFOLDER}/installation/"
sudo chmod +x install_raspaccess.sh
sudo ./install_raspaccess.sh


# Final feedback
echo "Installation completed!"
read -p "Press enter to reboot." nothing
sudo reboot now
