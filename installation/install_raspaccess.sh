#!/bin/bash

echo "Copy raspaccess.service..."
sudo cp raspaccess.service /lib/systemd/system/raspaccess.service
echo "Make raspaccess.service executable..."
sudo chmod 644 /lib/systemd/system/raspaccess.service
chmod +x /var/www/html/raspaccess/python/raspaccess.py
echo "Reload daemon..."
sudo systemctl daemon-reload
echo "Enable raspaccess.service..."
sudo systemctl enable raspaccess.service
echo "Start raspaccess.service..."
sudo systemctl start raspaccess.service
echo "Done!"
